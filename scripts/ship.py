import bge
import config
import mathutils



thrust = mathutils.Vector([0,0,0])
turn = 0
speed = mathutils.Vector([0,0,0])
energy = None


def init(cont):
    global energy
    cont.script = 'ship.update'
    cont.activate(cont.actuators['Air'])
    cont.owner['Y'] = cont.owner.worldPosition.copy().y
    if bge.logic.globalDict.get('gameType') == 'Fear Dark':
        energy = 100
    
    
def update(cont):
    global thrust, energy
    global turn
    global speed
    new_thrust = mathutils.Vector([0,0,0])
    new_turn = 0
    if cont.sensors['UP'].positive and (energy == None or energy > 0):
        new_thrust.y += 1
        if energy != None:
            energy -= 0.2
        
    if cont.sensors['LEFT'].positive:
        new_turn += 1
    if cont.sensors['RIGHT'].positive:
        new_turn -= 1
    
    thrust = (thrust * (1 - config.THRUST_RAMP) + new_thrust * config.THRUST_RAMP)
    turn = (turn * (1 - config.THRUST_RAMP) + new_turn * config.THRUST_RAMP)
    
    out_thrust = thrust * config.SHIP_POWER_WEIGHT_RATIO * cont.owner.mass
    drag = cont.owner.localLinearVelocity * config.SHIP_DRAG_RATIO
    net_force = out_thrust-drag
    
    out_turn = turn * config.SHIP_TURN_RATE

    cont.owner.applyForce(net_force,True)
    cont.owner.localAngularVelocity.z = out_turn
    
    accel = speed - cont.owner.worldLinearVelocity
    speed = cont.owner.worldLinearVelocity.copy()
    if accel.length > 3:
        clunk_sound = cont.actuators['Clunk']
        clunk_sound.volume = accel.length/8
        cont.activate(cont.actuators['Clunk'])
    cont.owner.worldPosition.y = cont.owner['Y']
          
    #Sound:
    air_controller = cont.actuators['Air']
    air_controller.volume = drag.length/40
    air_controller.pitch = drag.length/10+1
    
    #Light
    light_hooks = [o for o in cont.owner.scene.objects if ('LightOn' in o)]
    try:
        closest_hook = sorted([(cont.owner.getDistanceTo(o), o) for o in light_hooks])[0]
        if closest_hook[0] < 1.5 and closest_hook[1]['LightOn'] == False:
            closest_hook[1]['LightOn'] = True
            bge.logic.sendMessage("LIGHTON")
        if energy != None and closest_hook[1]['LightOn'] == True:
            energy += 1/(closest_hook[0])**2
            energy = min(energy, 100)
    except IndexError:
        pass
        
        
    #Death
    if energy != None and energy <= 0:
        if cont.owner.worldLinearVelocity.length < 0.01:
            launchpad = cont.owner.scene.objects['Launchpad']
            cont.owner.worldPosition.x = 0
            cont.owner.worldPosition.z = -0.9
            energy = 100
            
        
