import bge
import datetime 
def init(cont):
    start_time = bge.logic.globalDict['start_time'] 
    dt = (datetime.datetime.now() - start_time)
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    
    time_bonus = int(5*60*1000*1000/ms)
    complexity_bonus = bge.logic.globalDict['complexity']**2
    
    cont.owner.text = "Level "+str(bge.logic.globalDict['level'])+" Completed\n"
    #cont.owner.text += "Time Bonus: "+str(time_bonus)+"\n"
    #cont.owner.text += "Complexity: "+str(complexity_bonus)+"\n"
    cont.owner.text += "\n"
    cont.owner.text += "Score: "+str(time_bonus + complexity_bonus)
