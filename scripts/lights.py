import bge

available_light_list = []

def init(cont):
    global available_light_list
    available_light_list = [o for o in cont.owner.scene.objects if o.name.startswith("Lamp")]
    
    for l in available_light_list:
        l.energy = 0.0

    cont.script = 'lights.run'

def run(cont):
    light_hooks = [o for o in cont.owner.scene.objects if ('LightOn' in o and o['LightOn'] == True)]
    light_hooks = [(cont.owner.getDistanceTo(o), o) for o in light_hooks]
    
    to_have_lights = sorted(light_hooks)[0:3]
    for light in range(len(to_have_lights)):
        
        l = available_light_list[light]
        o = to_have_lights[light][1]
        o.children[0].worldScale = [10,10,10]
        
        l.worldPosition = o.worldPosition
        l.color = eval(o['col'])
        l.energy = 2.0
        l.distance = 1.0

        
        
