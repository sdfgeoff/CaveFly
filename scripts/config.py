THRUST_RAMP = 0.1 #Higher is more responsive
SHIP_POWER_WEIGHT_RATIO = 20
SHIP_DRAG_RATIO = 2
SHIP_TURN_RATE = 3

NUM_LIGHTS = 5
WATER_AMOUNT = 3


SIZE = 15


GAME_TYPES = ['Exploration', 'Fear Dark','Help', 'Quit']
