import bge
import config

index = 0

def init(cont):
    textObj = cont.owner.scene.objects['GameMode']
    bge.logic.globalDict['gameType'] = config.GAME_TYPES[index]
    textObj.text = config.GAME_TYPES[index]
    
    cont.script = 'menu.run'
    
def run(cont):
    if not cont.sensors['Message'].positive:
        return
    global index
    
    textObj = cont.owner.scene.objects['GameMode']
    for message in cont.sensors['Message'].subjects:
        if message == 'SCROLLLEFT':
            index -= 1
        elif message == 'SCROLLRIGHT':
            index += 1
        if message == 'STARTGAME':
            gametype = config.GAME_TYPES[index]
            if gametype == 'Quit':
                bge.logic.endGame()
            elif gametype == 'Help':
                bge.logic.addScene('HELP', 1)
            else:
                cont.activate(cont.actuators[0])
    
    index = wrap(index, len(config.GAME_TYPES) - 1, 0)
    textObj.text = config.GAME_TYPES[index]
    bge.logic.globalDict['gameType'] = config.GAME_TYPES[index]

def wrap(num, ma, mi):
    '''Wraps a number from max to min'''
    if num > ma:
        num -= (ma - mi)+1
    elif num < mi:
        num += (ma - mi)+1
    return num
