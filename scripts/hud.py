import bge
import ship

lights = 0
energy = 0

def init(cont):
    global lights
    cont.script = 'hud.run'
    lights = 0
    cont.owner.resolution = 5
    
UVSPEED = -0.001
    
def run(cont):
    global lights
    if cont.sensors['LIGHTON'].positive:
        lights += 1
    
    if lights != bge.logic.globalDict['total_lights']:
        cont.owner['Text'] = str(lights) + '/' + str(bge.logic.globalDict['total_lights'])
    elif  cont.owner['Text'] != 'Return Home':
        cont.owner['Text'] = 'Return Home'
        bge.logic.sendMessage("COMPLETE")

    global energy
    ship_energy = ship.energy
    if ship_energy == None:
        ship_energy = 0
    energy = (ship_energy*0.05 + energy*0.95)
    #print(energy)

    energy_meter =  cont.owner.scene.objects['EnergyMeter']
    energy_meter.color = [0.8*energy/100, 0.6*energy/100, 0.3*energy/100, energy]
    mesh = energy_meter.meshes[0]
    array = mesh.getVertexArrayLength(0)

    for v in range(0,array):
        vert = mesh.getVertex(0,v) 
        vert.v2 += UVSPEED
