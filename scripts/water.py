import bge

UVSPEED = 0.005

first = None

def init(cont):
    global first
    
    if first == None:
        first = "Done"
        cont.owner['First'] = True
    else :
        cont.owner['First'] = False
    
    cont.script = 'water.run'
    
def run(cont):

    if cont.owner['First'] == True:
        own = cont.owner
        mesh = own.meshes[0]
        array = mesh.getVertexArrayLength(0)
        for v in range(0,array):
            vert = mesh.getVertex(0,v) 
            vert.v += UVSPEED
