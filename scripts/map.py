import bge
import random
import math
import config

accessable_area = []
tiledict = {}

def init(cont):
    accessable_area = list()
    tiledict = {}
    #Build Tile dictionary
    for obj in cont.owner.scene.objectsInactive:
        if obj.name.startswith('T ['):
            add_shape(obj)
    
    #Map Size:
    bge.logic.globalDict['level'] = bge.logic.globalDict.get('level', 0) + 1
    if bge.logic.globalDict.get('gameType') == 'Fear Dark':
        bge.logic.globalDict['size'] = bge.logic.globalDict.get('size', 8) + 2
        config.SIZE = bge.logic.globalDict['size']

    else:
        bge.logic.globalDict['size'] = bge.logic.globalDict.get('size', 12) + 1
        config.SIZE = bge.logic.globalDict['size']
    
    #Generate Map
    map, offset = generate_map()
    
    #Build Map
    build_map(cont, map, offset)
                       
    cont.script = 'map.run'
    
def run(cont):
    pass

def generate_map():
    global accessable_area
    
    biggest = (0, [0,0], 0) #(Area, [coords], num)
    while(biggest[0] < config.SIZE **1.5):
        map = Map(config.SIZE)
        accessable_area = list()
        #Fill with Noise
        for r in range(0, map.size):
            for c in range(0, map.size):
                map.set_point(r,c,random.randint(0,1))
                
        #Remove walls
        for r in range(0, map.size):
            for c in range(0, map.size):
                if map.get_point(r,c) == map.WALL:
                    if (map.get_point(r+1,c) == map.get_point(r-1,c) and
                        map.get_point(r,c+1) == map.get_point(r,c-1) and
                        map.get_point(r+1,c) != map.get_point(r,c+1)):
                        map.set_point(r,c,map.EMPTY)
                        
        #Extract the playable area (which is the biggest):
        
        i = 3
        for x in range(0, map.size):
            for y in range(0, map.size):
                i += 1
                size = get_size(map,x,y, i)
                if size > biggest[0]:
                    biggest = (size, [x,y], i)
        for x in range(0, map.size):
            for y in range(0, map.size):
                if map.get_point(x,y) == biggest[2]:
                    accessable_area.append([x,y])
                if map.get_point(x,y) != map.WALL:
                    map.set_point(x,y,map.EMPTY)
    
        #Figure out start co-ordinates:
        offset = [None, None]
        for point in accessable_area:
            x,y = point
            if map.get_point(x, y-1) == map.WALL:
                offset = point
    
    #Place Water
    #for w in random.sample(accessable_area, config.WATER_AMOUNT):
    #    #if map.get_num_neighbours(w[0], w[1]) > 2:
    #    x,y = w
    #    if map.get_point(x,y-1) == 0:
    #        #print("Here")
    #        map.set_point(x,y,'Water')
    
    #Place Traps:
    
    bge.logic.globalDict['complexity'] = biggest[0]    
    
    return map, offset

def build_map(cont, map, offset):
    roof = []
    for y in range(-5,map.size+5):
        for x in range(-5,map.size+5):
            shape = [[map.get_point(x,y), map.get_point(x-1,y)],[map.get_point(x,y-1), map.get_point(x-1,y-1)]]
            
            pos = [(-x+offset[0])*2+1,(y-offset[1])*2-1]
            if shape[1] == [1,1]:
                if (shape[0][0] + shape[0][1] <= 1):
                    place_feature(cont, pos)
            elif (shape[0][0] + shape[0][1] >= 1):
                if [x,y] in accessable_area:        
                    roof.append([x,y])
            place_shape(cont, shape, pos)
    
    
    num_lights = min(len(roof), config.NUM_LIGHTS)
    for l in random.sample(roof, num_lights):
        x,y = l
        place_light(cont, [(-x+offset[0])*2,(y-offset[1])*2])
        
    bge.logic.globalDict['total_lights'] = num_lights

##################   PLACE OBJECTS  ###########################

def place_shape(cont, shape, pos):
    obj = get_shape(shape)
    o = bge.logic.getCurrentScene().addObject(obj[0], cont.owner)
    o.worldPosition.z = pos[1]
    o.worldPosition.x = pos[0]
    o.worldPosition.y = -1
    
    o.applyRotation([0,-obj[1]*math.pi/2,0])
    

def place_feature(cont, pos):
    if bge.logic.getRandomFloat() > 0.5:
        return
    feature = random.choice([o for o in cont.owner.scene.objectsInactive if o.name.startswith("Feature")])
    o = bge.logic.getCurrentScene().addObject(feature, cont.owner)
    o.worldPosition.z = pos[1]
    o.worldPosition.x = pos[0]
    o.worldPosition.y = -0.5
    o.color = (0,0,0,0.6)

def place_light(cont, pos):
    o = bge.logic.getCurrentScene().addObject('LightHook', cont.owner)
    o.worldPosition.z = pos[1]
    o.worldPosition.x = pos[0]
    o.worldPosition.y = -1.2
    

##################   TILE DICT  ###########################
def rotateTile(strshape, r):
    shape = eval(strshape)
    for i in range(0,r):
        tmp = shape[0][0]
        shape[0][0] = shape[0][1]
        shape[0][1] = shape[1][1]
        shape[1][1] = shape[1][0]
        shape[1][0] = tmp
    return str(shape)

def add_shape(obj):
    shape = obj.name[2:15]
    for r in range(0,4):
        s = rotateTile(shape, r)
        if s in tiledict:
            tiledict[s].append([obj, r])
            
        else:
            tiledict[s] = [[obj, r]]
            

def get_shape(shape):
    return(random.choice(tiledict[str(shape)]))

###################### MAP CLASS #######################3
class Map():
    EMPTY = 0
    WALL = 1
    size = 0
    def __init__(self, size):
        self.map = []
        self.size = size
        for r in range(0, self.size):
            self.map.append([])
            for c in range(0, self.size):
                self.map[r].append([])
                self.map[r][c] = 1

    def get_point(self, x, y):
        if x not in range(0, self.size) or y not in range(0, self.size):
            return self.WALL
        else:
            return self.map[x][y]
        
    def set_point(self, x, y, value):
        if x not in range(0, self.size) or y not in range(0, self.size):
            raise Exception('Trying to access co-ordinate not in map')
        else:
            self.map[x][y] = value
            
    def get_num_neighbours(self, x,y):
        total = 0
        for r in [-1,0,1]:
            for c in [-1,0,1]:
                total += self.get_point(x+r,y+c)
        return total
            
    def __str__(self):
        outstr = ''
        for r in range(0, self.size):
            if r != 0:
                outstr += '\n'
            for c in range(0, self.size):
                if self.map[r][c] == self.EMPTY:
                    outstr += '  '
                elif self.map[r][c] == self.WALL:
                    outstr += '[]'
                else:
                    outstr += str(self.map[r][c])
        return outstr
    
def get_size(map, x, y, i=0):
    '''A recursive function to find the size of a map area'''
    if map.get_point(x,y) == map.EMPTY:
        map.set_point(x,y,i)
        count = 1 + get_size(map, x+1, y, i) + get_size(map, x-1, y, i) + get_size(map, x, y+1, i) + get_size(map, x, y-1, i)
        return count
    else:
        return 0
