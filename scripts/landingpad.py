import bge

ended = False

def init(cont):
    cont.owner['light_pattern'] = [int(c) for c in cont.owner['light_pattern']]
    cont.owner['bright'] = 0.0
    cont.script = 'landingpad.run'
    cont.owner['complete'] = False
    
def run(cont):
    global ended
    #Light:
    if cont.owner['light_index'] >= len(cont.owner['light_pattern']):
        cont.owner['light_index'] = 0
    bright = cont.owner['light_pattern'][cont.owner['light_index']]
    
    cont.owner['bright'] = (cont.owner['bright'] * 0.5 + bright*0.5)
    bright = cont.owner['bright']
    cont.owner.children[0].color = [bright, bright, bright, 1]
    cont.owner.children[0].children[0].energy = bright*3
    
    if cont.sensors['Collision'].positive and cont.owner['complete'] == True:
        if cont.sensors['Collision'].hitObject.worldLinearVelocity.length <= 0.1:
            if ended == False:
                for act in cont.actuators:
                    cont.activate(act)
                bge.logic.sendMessage("FADEOUT")
                ended = True
    
    if cont.sensors['COMPLETE'].positive:
        cont.owner['complete'] = True
