import bge
import config
import ship

UVSPEED = 0.03


light = None
ramp = 0

def init(cont):
    global light
    cont.script = 'flame.update'
    light = cont.owner.children['Flame Light']
    cont.activate(cont.actuators['Boosters'])
    
def update(cont):
    global ramp
    nramp = 0
    
    own = cont.owner
    mesh = own.meshes[0]
    array = mesh.getVertexArrayLength(0)

    for v in range(0,array):
        vert = mesh.getVertex(0,v) 
        vert.u2 += UVSPEED
    

    if cont.sensors['ON'].positive and (ship.energy == None or ship.energy) > 0:
        nramp += 1
    
    ramp = (ramp * (1 - config.THRUST_RAMP) + nramp * config.THRUST_RAMP)
    
    light.energy = (bge.logic.getRandomFloat()*1 + 5)*ramp
    light.spotsize = bge.logic.getRandomFloat()*90 + 60
    cont.owner.color = (ramp**2,ramp**2,ramp**2,ramp**2)
    
    #Audio:
    sound_controller = cont.actuators['Boosters']
    sound_controller.volume = ramp/2
    sound_controller.pitch = 1/(max(0.2, ramp)**1.1)
        
    
