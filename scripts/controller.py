#Button -> Control converter
#Includes camera control

#Does Music as well?

import bge
import mathutils
import datetime
import random


import config

SHAKE_SCALE = 0.03


start_time = 0
shake = 0
music = None
volume = 0
active = 0

def init(cont):
    global start_time
    global music, active
    cont.script = 'controller.update'
    start_time = datetime.datetime.now()
    bge.logic.globalDict['start_time'] = start_time
    cont.activate(cont.actuators['HUD'])
    music = random.choice([a for a in cont.actuators if a.name.startswith("Music")])
    active = 1
    bge.render.showMouse(False)

def update(cont):
    global shake, active
    new_shake = 0
    if active == 1:
        for sensor in cont.sensors:
            if sensor.name.startswith('UP') and sensor.positive:
                bge.logic.sendMessage("UP")
                new_shake += 1
            elif sensor.name.startswith('LEFT') and sensor.positive:
                bge.logic.sendMessage("LEFT")
            elif sensor.name.startswith('RIGHT') and sensor.positive:
                bge.logic.sendMessage("RIGHT")
      
      
    #Shake  
    shake = (shake * (1 - config.THRUST_RAMP) + new_shake * config.THRUST_RAMP)
    
    cont.owner.localPosition.x = (bge.logic.getRandomFloat()-0.5)*shake*SHAKE_SCALE
    cont.owner.localPosition.z = (bge.logic.getRandomFloat()-0.5)*shake*SHAKE_SCALE
    
    #Camera Filter:
    dt = (datetime.datetime.now() - start_time)
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    alpha = max(0, 1 - ms/1000)
    cont.owner.children['Filter'].color = (0,0,0,alpha)
    
    #Music:
    global volume
    cont.activate(music)
    volume = (volume*0.9 + active*0.1)
    music.volume = 0#volume/5
    if cont.sensors['FADEOUT'].positive:
        active = 0
